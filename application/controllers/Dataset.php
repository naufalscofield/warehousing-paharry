<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('rens_model');
	}

	public function index()
	{
		$data['parameter'] = $this->rens_model->getParameter();
		$data['dataset'] = $this->rens_model->getDataset();
	
		$this->load->view('components/header');
		$this->load->view('dataset',$data);
		$this->load->view('components/footer');

	}

	
}
