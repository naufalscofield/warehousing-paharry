        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, template made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
                </div>
            </div>
        </footer>

    </div>
</div>
</body>

	<script src="<?= base_url();?>assets/js/admin/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?= base_url();?>assets/js/admin/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="<?= base_url();?>assets/js/admin/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?= base_url();?>assets/js/admin/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="<?= base_url();?>assets/js/admin/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="<?= base_url();?>assets/js/admin/demo.js"></script>
</html>
