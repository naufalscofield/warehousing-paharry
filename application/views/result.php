<div class="sidebar" data-background-color="white" data-active-color="danger">

        <div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="<?= base_url();?>/">
                        <i class="fa fa-tachometer"></i>
                        <p>Naive Bayes</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>index.php/Dataset">
                        <i class="fa fa-th"></i>
                        <p>Dataset</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>index.php/parameter">
                        <i class="fa fa-tasks"></i>
                        <p>Parameter</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>index.php/supplier">
                        <i class="fa fa-shopping-cart"></i>
                        <p>Supplier</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>decision">
                        <i class="fa fa-question"></i>
                        <p>Decision</p>
                    </a>
                </li>
            </ul>
        </div>
</div>
<div class="main-panel">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                </button>
                <a class="navbar-brand">Result Forecast</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                       
                    </li>
                </ul>

            </div>
        </div>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="content table-responsive ">
                            <form id="add" action="<?= base_url();?>index.php/welcome/forecast" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php 
                                            $sales = $this->session->userdata('sales');

                                            if ($sales == 'QUIET'){
                                                $unsales = 'CROWDED';
                                            }else{
                                                $unsales = 'QUIET';
                                            }
                                            ?>
                                            <center><label><b>The sales forecasted will be <?= $sales; ?></b></label></center><br>
                                            <center><label><b>Prior of <?= $sales; ?> =  <?= $this->session->userdata('prior'); ?></b></label></center><br>
                                            <center><label><b>likelihood of <?= $sales; ?> =  <?= $this->session->userdata('likelihood'); ?></b></label></center><br>
                                            <center><label><b>Probability of <?= $sales; ?> =  <?= $this->session->userdata('probability_ok'); ?></b></label>
                                                    <label>------------Probability of <?= $unsales; ?> =  <?= $this->session->userdata('probability_no'); ?></label>
                                            </center><br>
                                            <center><label><b>RENS are sugesting for you to stock these items :</b></label></center><br>
                                            <table id="" class="table table-striped">
                                            <thead>
                                                <tr>
                                                <th width="20"><center><b>Product</b></center></th>
                                                <th width="20"><center><b>Amount</b></center></th>
                                                </tr>
                                            </thead>

                                            <?php $m = $this->db->get('supplier')->result_array();
                                            $status = $this->session->userdata('status');
                                            ?>

                                            <tbody id="table-row">
                                            <tr>
                                                <?php foreach ($m as $message) { ?>
                                                <td width="20"><center><b><?= $message['product']; ?></b></center></td>
                                                <td width="20"><center><b><?= $message[$status]?> <?= $message['product_units']; ?></b></center></td>
                                            </tr>
                                            </tbody>
                                              <?php  } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="modal-footer">
                                <center>
                                </center>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="tambahBarang" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add" action="<?= base_url();?>welcome/insertBarang" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input type="text" required id="nama_barang" class="form-control border-input" placeholder="Nama Barang" value="" name="nama_barang">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Pilih Lorong</label>
                                    <select required name="id_lorong" class="form-control border-input" id="select_lorong">
                                        <option value="">--Pilih Lorong--</option>
                                        <?php 
                                                foreach ($lorong as $key) { ?>
                                            <option value="<?= $key['id'];?>">
                                                <?= $key['nama_lorong'];?>
                                            </option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Pilih Rak</label>
                                    <select required name="id_rak" class="form-control border-input" id="select_rak">
                                        <option value="">-- --</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                Tambah Barang
                            </button>
                            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="editBarang" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="update" method="post" action="<?= base_url();?>welcome/updateBarang">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ID</label>
                                    <input type="hidden" required id="input_id" class="form-control border-input" placeholder="Nama Barang" value="" name="id">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input type="text" required id="input_nama_barang" class="form-control border-input" placeholder="Nama Barang" value="" name="nama_barang">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Pilih Lorong</label>
                                    <select required name="id_lorong" class="form-control border-input" id="input_select_lorong">
                                        <option value="">--Pilih Lorong--</option>
                                        <?php 
                                                foreach ($lorong as $key) { ?>
                                            <option value="<?= $key['id'];?>">
                                                <?= $key['nama_lorong'];?>
                                            </option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Pilih Rak</label>
                                    <select required name="id_rak" class="form-control border-input" id="input_select_rak">
                                        <option value="">-- --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-fill btn-wd">
                                Edit Barang
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url ()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        // $('#myTable').DataTable();

        $('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
        });

        $('#select_lorong').on('change', function() {
            // jika kelas dirubah
            console.log('aw');
            $.ajax({
                type: "POST",
                data: {
                    id_lorong: $('#select_lorong').val()
                },
                url: '<?php echo base_url()."welcome/getRak" ?>',
                dataType: 'text',
                success: function(resp) {
                    console.log('respon', resp)
                    var json = JSON.parse(resp.replace(',.', ''))
                    var $el = $("#select_rak");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Rak --'));
                    $.each(json, function(key, value) {
                        $el.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_rak));
                    });
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR, exception)
                }
            });
        });

        $('#input_select_lorong').on('change', function() {
            // jika kelas dirubah
            console.log('aw');
            $.ajax({
                type: "POST",
                data: {
                    id_lorong: $('#input_select_lorong').val()
                },
                url: '<?php echo base_url()."welcome/getRak" ?>',
                dataType: 'text',
                success: function(resp) {
                    console.log('respon', resp)
                    var json = JSON.parse(resp.replace(',.', ''))
                    var $el = $("#input_select_rak");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                        .attr("value", '').text('-- Pilih Rak --'));
                    $.each(json, function(key, value) {
                        $el.append($("<option></option>")
                            .attr("value", value.id).text(value.nama_rak));
                    });
                },
                error: function(jqXHR, exception) {
                    console.log(jqXHR, exception)
                }
            });
        });

        $(document).on('click', '#btn_edit', function() {
            // console.log($(this).data("nama_barang_edit"));
            var id = $(this).data("id_edit");
            var nama_barang = $(this).data("nama_barang_edit")
            console.log(id, nama_barang)
            $('#input_id').val(id)
            $('#input_nama_barang').val(nama_barang)
        });

    });
</script>
@endsection